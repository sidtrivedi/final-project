﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace ValidationLibrary
{
    public class RequiredValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString() == string.Empty || value == null)
            {
                return new ValidationResult(false, "Field is required");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class PhoneValidation : ValidationRule
    {
        Regex regexRule = new Regex(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}");
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (regexRule.IsMatch(value.ToString()))
            {
                return ValidationResult.ValidResult;
            }
            else
            {
                return new ValidationResult(false, "Invalid Phone Number. Should be 123 456-7890");
            }
        }
    }

    public class EmailValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            Regex emailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (emailRegex.IsMatch(value.ToString()))
            {
                return ValidationResult.ValidResult;
            }
            else
            {
                return new ValidationResult(false, "Invalid Email Address. Should be email@host.ext");
            }
        }
    }
}
