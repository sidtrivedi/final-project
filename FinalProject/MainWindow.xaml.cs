﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using ValidationLibrary;

namespace FinalProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Department> departments;
        ObservableCollection<Schedule> schedules;
        List<TimeSlot> timeSlots;
        bool filtering;
        public List<Department> Departments { get => departments; set => departments = value; }
        public ObservableCollection<Schedule> Schedules { get => schedules; set => schedules = value; }

        public MainWindow()
        {
            filtering = true;
            Schedules = new ObservableCollection<Schedule>();
            LoadXMlData(Schedules);
            InitializeComponent();
            cmbBoxFilterBy.SelectedIndex = 0;
            Departments = new List<Department>();
            List<Interviewer> interviewers = new List<Interviewer>();
            timeSlots = new List<TimeSlot>();
            for(int i =10; i < 18; i+=2)
            {
                timeSlots.Add(new TimeSlot(i, i+2));
            }
            interviewers.Add(new Interviewer("John", "Smith", timeSlots));
            interviewers.Add(new Interviewer("Bill", "Clinton", timeSlots));
            interviewers.Add(new Interviewer("George", "Bush", timeSlots));
            Departments.Add(new InfoTech(interviewers));

            interviewers = new List<Interviewer>();
            interviewers.Add(new Interviewer("Richard", "Nixon", timeSlots));
            interviewers.Add(new Interviewer("Ronald", "Reagan", timeSlots));
            interviewers.Add(new Interviewer("Andrew", "Jackson", timeSlots));
            Departments.Add(new HumanResource(interviewers));

            interviewers = new List<Interviewer>();
            interviewers.Add(new Interviewer("John", "McCain", timeSlots));
            interviewers.Add(new Interviewer("Theodore", "Roosevelt", timeSlots));
            Departments.Add(new Maintenance(interviewers));

            this.DataContext = this;

            schedules.CollectionChanged += Schedules_CollectionChanged;

            //LoadXMlData(Schedules);
        }

        void ValidateControls()
        {
            txtBxCName.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            txtBxCEmail.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            txtBxCContact.GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private bool IsAlreadyScheduled(Schedule schedule)
        {
            if(!Validation.GetHasError(txtBxCName) && !Validation.GetHasError(txtBxCEmail) && !Validation.GetHasError(txtBxCContact) && txtBxCContact.Foreground == Brushes.Black)
            {
                string interviewer = schedule.Interviewer, date = schedule.Date, timeSlot = schedule.Timeslot;
                return schedules.Any(
                    x => x.Interviewer.Equals(interviewer) &&
                    x.Date.Equals(date) &&
                    x.Timeslot.Equals(timeSlot)
                    );
            }
            return false;
        }

        private void LoadXMlData(Collection<Schedule> collectionSchedules )
        {
            string filePath = "Schedules.xml";
            if (!File.Exists(filePath)) return;
            StreamReader streamReader = new StreamReader(filePath);
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ScheduleList));
                ScheduleList scheduleList = xmlSerializer.Deserialize(streamReader) as ScheduleList;
                streamReader.Close();
                if (scheduleList != null)
                {
                    foreach (Schedule schedule in scheduleList.Schedules)
                    {
                        collectionSchedules.Add(schedule);
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Exception occured during deserializing xml file." + exc.InnerException);
            }
            finally
            {
                streamReader.Close();
            }
        }
        private void Schedules_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (filtering) return;
            TextWriter textWriter = new StreamWriter("Schedules.xml");
            try
            {
                ScheduleList scheduleList = new ScheduleList(schedules);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(ScheduleList));
                xmlSerializer.Serialize(textWriter, scheduleList);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Exception occured during serializing data to xml file." + exc.InnerException);
            }
            finally
            {
                textWriter.Close();
            }
        }

        private Schedule CollectScheduleInfo()
        {
            if (!Validation.GetHasError(txtBxCContact) && !Validation.GetHasError(txtBxCEmail) && !Validation.GetHasError(txtBxCName) && txtBxCContact.Foreground == Brushes.Black)
            {
                string department, interviewer, timeslot, assessmentType = "";
                DateTime date = (DateTime)dtPicker.SelectedDate;
                Candidate candidate = new Candidate(
                    txtBxCName.Text.Trim(),
                    txtBxCContact.Text.Substring(0, 10),
                    txtBxCEmail.Text.Trim()
                );
                department = (cmbBxDepartment.SelectedItem as Department).Name.ToString();
                interviewer = (cmbBxInterviwer.SelectedItem as Interviewer).FullName.ToString();
                timeslot = (cmbBxTimeSlot.SelectedItem as TimeSlot).TimeRangeString;
                switch (department)
                {
                    case "Information Technology":
                        assessmentType = (cmbBxDepartment.SelectedItem as InfoTech).AssignAssessment();
                        break;
                    case "Human Resource":
                        assessmentType = (cmbBxDepartment.SelectedItem as HumanResource).AssignAssessment();
                        break;
                    case "Maintenance":
                        assessmentType = (cmbBxDepartment.SelectedItem as Maintenance).AssignAssessment();
                        break;
                }

                return new Schedule(department, interviewer, date, timeslot, candidate, assessmentType);
            }
            else return null;
        }

        private void btnSchedule_Click(object sender, RoutedEventArgs e)
        {
            ValidateControls();
            if (!Validation.GetHasError(txtBxCContact) && !Validation.GetHasError(txtBxCName) && !Validation.GetHasError(txtBxCEmail) && txtBxCContact.Foreground == Brushes.Black)
            {
                Schedule newSchedule = CollectScheduleInfo();
                if (IsAlreadyScheduled(newSchedule))
                {
                    MessageBox.Show("Can't schedule as the interviewer has already been scheduled for the same time period on the same date.");
                    return;
                }
                filtering = false;
                Schedules.Add(newSchedule);
                filtering = true;
            }
            //MessageBox.Show(String.Format("{0}, {1}, {2}, {3}, {4}", department, interviewer, date, timeslot, assessmentType));
        }

        private void dtPicker_Loaded(object sender, RoutedEventArgs e)
        {
            dtPicker.DisplayDateStart = DateTime.Now;
            dtPicker.DisplayDateEnd = DateTime.Now.AddDays(5);
            dtPicker.SelectedDate = DateTime.Now;
        }

        private void ToggleBtnVisibility(Button btn1, Button btn2)
        {
            btn1.Visibility = Visibility.Visible;
            btn2.Visibility = Visibility.Hidden;
            //btnModify.Visibility = btnModify.Visibility.Equals(Visibility.Visible) ? Visibility.Hidden : Visibility.Visible;
            //btnSchedule.Visibility = btnModify.Visibility.Equals(Visibility.Visible) ? Visibility.Hidden : Visibility.Visible;
        }
        private void Modify(object sender, RoutedEventArgs e)
        {
            try
            {
                Schedule selectedSchedule = dataGridSchedules.SelectedItem as Schedule;
                Department selectedDepartment = departments.First(x => x.Name.Equals(selectedSchedule.Department));
                Interviewer selectedInterviewer = selectedDepartment.Interviewers.First(x => x.FullName.Equals(selectedSchedule.Interviewer));
                DateTime selectedDate = DateTime.Parse(selectedSchedule.Date);
                TimeSlot selectedTimeSlot = timeSlots.First(x => x.TimeRangeString.Equals(selectedSchedule.Timeslot));
                cmbBxDepartment.SelectedItem = selectedDepartment;
                cmbBxInterviwer.SelectedItem = selectedInterviewer;
                dtPicker.SelectedDate = selectedDate;
                cmbBxTimeSlot.SelectedItem = selectedTimeSlot;
                txtBxCName.Text = selectedSchedule.Candidate.Name;
                txtBxCContact.Text = selectedSchedule.Candidate.Contact;
                txtBxCEmail.Text = selectedSchedule.Candidate.Email;
                ToggleBtnVisibility(btnModify, btnSchedule);
            }
            catch(Exception exc)
            {
                MessageBox.Show("An unexpected error occured." + exc.InnerException);
            }
            finally
            {
                ValidateControls();
            }
        }

        private void btnModify_Click(object sender, RoutedEventArgs e)
        {
            Schedule newSchedule = CollectScheduleInfo();
            Schedule oldSchedule = dataGridSchedules.SelectedItem as Schedule;
            if (IsAlreadyScheduled(newSchedule))
            {
                if(newSchedule.Candidate.Name == oldSchedule.Candidate.Name && newSchedule.Candidate.Contact == oldSchedule.Candidate.Contact && newSchedule.Candidate.Email == oldSchedule.Candidate.Email)
                {
                    MessageBox.Show("Can't schedule as the interviewer has already been scheduled for the same time period on the same date.");
                    return;
                }
            }
            ValidateControls();
            if (!Validation.GetHasError(txtBxCContact) && !Validation.GetHasError(txtBxCName) && !Validation.GetHasError(txtBxCEmail) && txtBxCContact.Foreground == Brushes.Black)
            {
                int indx = schedules.IndexOf(oldSchedule);
                filtering = false;
                if (indx != -1) schedules[indx] = newSchedule;
                filtering = true;
                ToggleBtnVisibility(btnSchedule, btnModify);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            txtBxCContact.Clear();
            txtBxCName.Clear();
            txtBxCEmail.Clear();
            ToggleBtnVisibility(btnSchedule, btnModify);
        }

        private void cmbBoxFilterBy_Selected(object sender, RoutedEventArgs e)
        {
            filtering = true;
            Collection<Schedule> scheduleList = new Collection<Schedule>();
            LoadXMlData(scheduleList);
            IOrderedEnumerable<Schedule> query;
            string selectedDepartment = (cmbBoxFilterBy.SelectedItem as ComboBoxItem).Content.ToString();
            if (selectedDepartment.Equals("Interviewer"))
            {
                query = from schedule in scheduleList
                        where schedule.Interviewer.Contains(txtBxFilter.Text)
                        orderby schedule.Interviewer ascending
                        select schedule;
            }
            else if (selectedDepartment.Equals("Date"))
            {
                query = from schedule in scheduleList
                        where schedule.Date.Contains(txtBxFilter.Text)
                        orderby schedule.Date ascending
                        select schedule;
            }

            else if (selectedDepartment.Equals("Time Slot"))
            {
                query = from schedule in scheduleList
                        where schedule.Timeslot.Contains(txtBxFilter.Text)
                        orderby schedule.Timeslot ascending
                        select schedule;
            }

            else if (selectedDepartment.Equals("Assessment Type"))
            {
                query = from schedule in scheduleList
                        where schedule.AssessmentType.Contains(txtBxFilter.Text)
                        orderby schedule.AssessmentType ascending
                        select schedule;
            }

            else if (selectedDepartment.Equals("Candidate Name"))
            {
                query = from schedule in scheduleList
                        where schedule.Candidate.Name.Contains(txtBxFilter.Text)
                        orderby schedule.Candidate.Name ascending
                        select schedule;
            }
            else if (selectedDepartment.Equals("Candidate Phone"))
            {
                query = from schedule in scheduleList
                        where schedule.Candidate.Contact.Contains(txtBxFilter.Text)
                        orderby schedule.Candidate.Contact ascending
                        select schedule;
            }
            else if (selectedDepartment.Equals("Candidate Email"))
            {
                query = from schedule in scheduleList
                        where schedule.Candidate.Email.Contains(txtBxFilter.Text)
                        orderby schedule.Candidate.Email ascending
                        select schedule;
            }
            else
            {
                query = from schedule in scheduleList
                        where schedule.Department.Contains(txtBxFilter.Text)
                        orderby schedule.Department ascending
                        select schedule;
            }
            Schedules.Clear();
            foreach (var schedule in query)
                Schedules.Add(schedule);
        }

        public void DeleteSchedule(object sender, RoutedEventArgs e)
        {
            var s = dataGridSchedules.SelectedItem;
            filtering = false;
            Schedules.Remove(s as Schedule);
            filtering = true;

        }
    }
}
