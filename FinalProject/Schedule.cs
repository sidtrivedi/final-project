﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Schedule
    {
        private string department, interviewer, date, timeslot, assessmentType;
        private Candidate candidate;

        public string Department { get => department; set => department = value; }
        public string Interviewer { get => interviewer; set => interviewer = value; }
        public string Timeslot { get => timeslot; set => timeslot = value; }
        public Candidate Candidate { get => candidate; set => candidate = value; }
        public string Date { get => date; set => date = value; }
        public string AssessmentType { get => assessmentType; set => assessmentType = value; }

        public Schedule() { }

        public Schedule(string department, string interviewer, DateTime date,
            string timeslot, Candidate candidate, string assessmentType)
        {
            this.Department = department;
            this.Interviewer = interviewer;
            this.Date = date.Date.ToShortDateString();
            this.Timeslot = timeslot;
            this.Candidate = candidate;
            this.AssessmentType = assessmentType;
        }
    }
}
