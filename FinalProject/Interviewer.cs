﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Interviewer
    {
        private string firstName, lastName;
        private List<TimeSlot> timeSlots;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }

        public string FullName { get => firstName + " " + lastName; }

        public List<TimeSlot> TimeSlots { get => timeSlots; set => timeSlots = value; }

        public Interviewer(string firstName, string lastName, List<TimeSlot> timeSlots)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.TimeSlots = timeSlots;
        }
    }
}
