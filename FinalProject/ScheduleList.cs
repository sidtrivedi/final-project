﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FinalProject
{
    [XmlRoot("ScheduleList")]

    public class ScheduleList
    {
        [XmlArray("Schedules")]
        [XmlArrayItem("Schedule")]
        public List<Schedule> Schedules { get; set; }

        public ScheduleList() { }

        public ScheduleList(ObservableCollection<Schedule> schedules)
        {
            this.Schedules = schedules.ToList();
        }
    }
}
