﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Candidate
    {
        private string name, contact, email;

        public string Name { get => name; set => name = value; }
        public string Contact { get => contact; set => contact = value; }
        public string Email { get => email; set => email = value; }

        public Candidate() {
            Name = "";
            contact = "";
            email = "";
        }
        public Candidate(string name, string contact, string email)
        {
            this.Name = name;
            this.Contact = contact;
            this.Email = email;
        }
    }
}
