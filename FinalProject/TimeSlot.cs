﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class TimeSlot
    {
        private int fromTime, toTime;
        private bool available;

        public int FromTime { get => fromTime; set => fromTime = value; }
        public int ToTime { get => toTime; set => toTime = value; }
        public string TimeRangeString { get => String.Format(
            "{0}:00 to {1}:00", fromTime.ToString(), toTime.ToString());
        }
        
        public bool Available { get => available; set => available = value; }

        public TimeSlot(int fromTime, int toTime, bool available=true)
        {
            this.FromTime = fromTime;
            this.ToTime = toTime;
            this.Available = available;
        }
    }
}
