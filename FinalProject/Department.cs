﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject
{
    public class Department
    {
        private string name;
        private List<Interviewer> interviewers;

        public string Name { get => name; set => name = value; }
        public List<Interviewer> Interviewers { get => interviewers; set => interviewers = value; }

        public virtual string AssignAssessment() {
            return "Default Assessment";
        }
    }

    public class InfoTech : Department
    {
        public InfoTech(List<Interviewer> interviewers)
        {
            base.Name = "Information Technology";
            base.Interviewers = interviewers;

        }
        public new string AssignAssessment() => "Programming Assessment";
    }

    public class Maintenance : Department
    {
        public Maintenance(List<Interviewer> interviewers)
        {
            base.Name = "Maintenance";
            base.Interviewers = interviewers;
        }
        public new string AssignAssessment() => "DevOps Assessment";
    }

    public class HumanResource : Department
    {
        public HumanResource(List<Interviewer> interviewers)
        {
            base.Name = "Human Resource";
            base.Interviewers = interviewers;
        }

        public new string AssignAssessment() => "Management Assessment";
    }
}
